<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\ActivityType $activityType
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Activity Type'), ['action' => 'edit', $activityType->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Activity Type'), ['action' => 'delete', $activityType->id], ['confirm' => __('Are you sure you want to delete # {0}?', $activityType->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Activity Types'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Activity Type'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Activities'), ['controller' => 'Activities', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Activity'), ['controller' => 'Activities', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="activityTypes view large-9 medium-8 columns content">
    <h3><?= h($activityType->title) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Title') ?></th>
            <td><?= h($activityType->title) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Description') ?></th>
            <td><?= h($activityType->description) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($activityType->id) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Activities') ?></h4>
        <?php if (!empty($activityType->activities)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Title') ?></th>
                <th scope="col"><?= __('Description') ?></th>
                <th scope="col"><?= __('Activity Category Id') ?></th>
                <th scope="col"><?= __('Value1') ?></th>
                <th scope="col"><?= __('Value2') ?></th>
                <th scope="col"><?= __('Value3') ?></th>
                <th scope="col"><?= __('Value4') ?></th>
                <th scope="col"><?= __('File1') ?></th>
                <th scope="col"><?= __('File2') ?></th>
                <th scope="col"><?= __('Activity Type Id') ?></th>
                <th scope="col"><?= __('Points') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($activityType->activities as $activities): ?>
            <tr>
                <td><?= h($activities->id) ?></td>
                <td><?= h($activities->title) ?></td>
                <td><?= h($activities->description) ?></td>
                <td><?= h($activities->activity_category_id) ?></td>
                <td><?= h($activities->value1) ?></td>
                <td><?= h($activities->value2) ?></td>
                <td><?= h($activities->value3) ?></td>
                <td><?= h($activities->value4) ?></td>
                <td><?= h($activities->file1) ?></td>
                <td><?= h($activities->file2) ?></td>
                <td><?= h($activities->activity_type_id) ?></td>
                <td><?= h($activities->points) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Activities', 'action' => 'view', $activities->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Activities', 'action' => 'edit', $activities->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Activities', 'action' => 'delete', $activities->id], ['confirm' => __('Are you sure you want to delete # {0}?', $activities->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
