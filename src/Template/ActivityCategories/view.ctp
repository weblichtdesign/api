<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\ActivityCategory $activityCategory
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Activity Category'), ['action' => 'edit', $activityCategory->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Activity Category'), ['action' => 'delete', $activityCategory->id], ['confirm' => __('Are you sure you want to delete # {0}?', $activityCategory->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Activity Categories'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Activity Category'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Activities'), ['controller' => 'Activities', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Activity'), ['controller' => 'Activities', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="activityCategories view large-9 medium-8 columns content">
    <h3><?= h($activityCategory->title) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Title') ?></th>
            <td><?= h($activityCategory->title) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Description') ?></th>
            <td><?= h($activityCategory->description) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($activityCategory->id) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Activities') ?></h4>
        <?php if (!empty($activityCategory->activities)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Title') ?></th>
                <th scope="col"><?= __('Description') ?></th>
                <th scope="col"><?= __('Activity Category Id') ?></th>
                <th scope="col"><?= __('Value1') ?></th>
                <th scope="col"><?= __('Value2') ?></th>
                <th scope="col"><?= __('Value3') ?></th>
                <th scope="col"><?= __('Value4') ?></th>
                <th scope="col"><?= __('File1') ?></th>
                <th scope="col"><?= __('File2') ?></th>
                <th scope="col"><?= __('Activity Type Id') ?></th>
                <th scope="col"><?= __('Points') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($activityCategory->activities as $activities): ?>
            <tr>
                <td><?= h($activities->id) ?></td>
                <td><?= h($activities->title) ?></td>
                <td><?= h($activities->description) ?></td>
                <td><?= h($activities->activity_category_id) ?></td>
                <td><?= h($activities->value1) ?></td>
                <td><?= h($activities->value2) ?></td>
                <td><?= h($activities->value3) ?></td>
                <td><?= h($activities->value4) ?></td>
                <td><?= h($activities->file1) ?></td>
                <td><?= h($activities->file2) ?></td>
                <td><?= h($activities->activity_type_id) ?></td>
                <td><?= h($activities->points) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Activities', 'action' => 'view', $activities->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Activities', 'action' => 'edit', $activities->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Activities', 'action' => 'delete', $activities->id], ['confirm' => __('Are you sure you want to delete # {0}?', $activities->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
