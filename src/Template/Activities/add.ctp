<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Activities'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Activity Categories'), ['controller' => 'ActivityCategories', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Activity Category'), ['controller' => 'ActivityCategories', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Activity Types'), ['controller' => 'ActivityTypes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Activity Type'), ['controller' => 'ActivityTypes', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="activities form large-9 medium-8 columns content">
    <?= $this->Form->create($activity ) ?>
    <fieldset>
        <legend><?= __('Add Activity') ?></legend>
        <?php
            echo $this->Form->control('title');
            echo $this->Form->control('description');
            echo $this->Form->control('activity_category_id', ['options' => $activityCategories, 'empty' => true]);
            echo $this->Form->control('activity_type_id', ['options' => $activityTypes, 'empty' => true]);
            echo $this->Form->control('points');

            echo $this->Form->control('value1');
            echo $this->Form->control('value2');
            echo $this->Form->control('value3');
            echo $this->Form->control('value4');
            echo $this->Form->control('file1');
            echo $this->Form->control('file2');

        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
