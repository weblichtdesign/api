<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\Activity $activity
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Activity'), ['action' => 'edit', $activity->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Activity'), ['action' => 'delete', $activity->id], ['confirm' => __('Are you sure you want to delete # {0}?', $activity->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Activities'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Activity'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Activity Categories'), ['controller' => 'ActivityCategories', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Activity Category'), ['controller' => 'ActivityCategories', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Activity Types'), ['controller' => 'ActivityTypes', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Activity Type'), ['controller' => 'ActivityTypes', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="activities view large-9 medium-8 columns content">
    <h3><?= h($activity->title) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Title') ?></th>
            <td><?= h($activity->title) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Description') ?></th>
            <td><?= h($activity->description) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Activity Category') ?></th>
            <td><?= $activity->has('activity_category') ? $this->Html->link($activity->activity_category->title, ['controller' => 'ActivityCategories', 'action' => 'view', $activity->activity_category->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Value1') ?></th>
            <td><?= h($activity->value1) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Value2') ?></th>
            <td><?= h($activity->value2) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Value3') ?></th>
            <td><?= h($activity->value3) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Value4') ?></th>
            <td><?= h($activity->value4) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('File1') ?></th>
            <td><?= h($activity->file1) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('File2') ?></th>
            <td><?= h($activity->file2) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Activity Type') ?></th>
            <td><?= $activity->has('activity_type') ? $this->Html->link($activity->activity_type->title, ['controller' => 'ActivityTypes', 'action' => 'view', $activity->activity_type->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($activity->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Points') ?></th>
            <td><?= $this->Number->format($activity->points) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Users') ?></h4>
        <?php if (!empty($activity->users)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Username') ?></th>
                <th scope="col"><?= __('Email') ?></th>
                <th scope="col"><?= __('Password') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($activity->users as $users): ?>
            <tr>
                <td><?= h($users->id) ?></td>
                <td><?= h($users->username) ?></td>
                <td><?= h($users->email) ?></td>
                <td><?= h($users->password) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Users', 'action' => 'view', $users->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Users', 'action' => 'edit', $users->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Users', 'action' => 'delete', $users->id], ['confirm' => __('Are you sure you want to delete # {0}?', $users->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
