<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\Activity[]|\Cake\Collection\CollectionInterface $activities
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Activity'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Activity Categories'), ['controller' => 'ActivityCategories', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Activity Category'), ['controller' => 'ActivityCategories', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Activity Types'), ['controller' => 'ActivityTypes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Activity Type'), ['controller' => 'ActivityTypes', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="activities index large-9 medium-8 columns content">
    <h3><?= __('Activities') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('title') ?></th>
                <th scope="col"><?= $this->Paginator->sort('description') ?></th>
                <th scope="col"><?= $this->Paginator->sort('activity_category_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('value1') ?></th>
                <th scope="col"><?= $this->Paginator->sort('value2') ?></th>
                <th scope="col"><?= $this->Paginator->sort('value3') ?></th>
                <th scope="col"><?= $this->Paginator->sort('value4') ?></th>
                <th scope="col"><?= $this->Paginator->sort('file1') ?></th>
                <th scope="col"><?= $this->Paginator->sort('file2') ?></th>
                <th scope="col"><?= $this->Paginator->sort('activity_type_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('points') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($activities as $activity): ?>
            <tr>
                <td><?= $this->Number->format($activity->id) ?></td>
                <td><?= h($activity->title) ?></td>
                <td><?= h($activity->description) ?></td>
                <td><?= $activity->has('activity_category') ? $this->Html->link($activity->activity_category->title, ['controller' => 'ActivityCategories', 'action' => 'view', $activity->activity_category->id]) : '' ?></td>
                <td><?= h($activity->value1) ?></td>
                <td><?= h($activity->value2) ?></td>
                <td><?= h($activity->value3) ?></td>
                <td><?= h($activity->value4) ?></td>
                <td><?= h($activity->file1) ?></td>
                <td><?= h($activity->file2) ?></td>
                <td><?= $activity->has('activity_type') ? $this->Html->link($activity->activity_type->title, ['controller' => 'ActivityTypes', 'action' => 'view', $activity->activity_type->id]) : '' ?></td>
                <td><?= $this->Number->format($activity->points) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $activity->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $activity->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $activity->id], ['confirm' => __('Are you sure you want to delete # {0}?', $activity->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
