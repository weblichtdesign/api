<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $activitiesUser->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $activitiesUser->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Activities Users'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Activities'), ['controller' => 'Activities', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Activity'), ['controller' => 'Activities', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="activitiesUsers form large-9 medium-8 columns content">
    <?= $this->Form->create($activitiesUser) ?>
    <fieldset>
        <legend><?= __('Edit Activities User') ?></legend>
        <?php
            echo $this->Form->control('activity_id', ['options' => $activities, 'empty' => true]);
            echo $this->Form->control('user_id', ['options' => $users, 'empty' => true]);
            echo $this->Form->control('com');
            echo $this->Form->control('file1');
            echo $this->Form->control('value1');
            echo $this->Form->control('value2');
            echo $this->Form->control('value3');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
