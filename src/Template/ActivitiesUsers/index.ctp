<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\ActivitiesUser[]|\Cake\Collection\CollectionInterface $activitiesUsers
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Activities User'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Activities'), ['controller' => 'Activities', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Activity'), ['controller' => 'Activities', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="activitiesUsers index large-9 medium-8 columns content">
    <h3><?= __('Activities Users') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('activity_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('user_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('file1') ?></th>
                <th scope="col"><?= $this->Paginator->sort('value1') ?></th>
                <th scope="col"><?= $this->Paginator->sort('value2') ?></th>
                <th scope="col"><?= $this->Paginator->sort('value3') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($activitiesUsers as $activitiesUser): ?>
            <tr>
                <td><?= $this->Number->format($activitiesUser->id) ?></td>
                <td><?= $activitiesUser->has('activity') ? $this->Html->link($activitiesUser->activity->title, ['controller' => 'Activities', 'action' => 'view', $activitiesUser->activity->id]) : '' ?></td>
                <td><?= $activitiesUser->has('user') ? $this->Html->link($activitiesUser->user->id, ['controller' => 'Users', 'action' => 'view', $activitiesUser->user->id]) : '' ?></td>
                <td><?= h($activitiesUser->file1) ?></td>
                <td><?= h($activitiesUser->value1) ?></td>
                <td><?= h($activitiesUser->value2) ?></td>
                <td><?= h($activitiesUser->value3) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $activitiesUser->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $activitiesUser->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $activitiesUser->id], ['confirm' => __('Are you sure you want to delete # {0}?', $activitiesUser->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
