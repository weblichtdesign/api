<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\ActivitiesUser $activitiesUser
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Activities User'), ['action' => 'edit', $activitiesUser->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Activities User'), ['action' => 'delete', $activitiesUser->id], ['confirm' => __('Are you sure you want to delete # {0}?', $activitiesUser->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Activities Users'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Activities User'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Activities'), ['controller' => 'Activities', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Activity'), ['controller' => 'Activities', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="activitiesUsers view large-9 medium-8 columns content">
    <h3><?= h($activitiesUser->user_id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Activity') ?></th>
            <td><?= $activitiesUser->has('activity') ? $this->Html->link($activitiesUser->activity->title, ['controller' => 'Activities', 'action' => 'view', $activitiesUser->activity->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('User') ?></th>
            <td><?= $activitiesUser->has('user') ? $this->Html->link($activitiesUser->user->id, ['controller' => 'Users', 'action' => 'view', $activitiesUser->user->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('File1') ?></th>
            <td><?= h($activitiesUser->file1) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Value1') ?></th>
            <td><?= h($activitiesUser->value1) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Value2') ?></th>
            <td><?= h($activitiesUser->value2) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Value3') ?></th>
            <td><?= h($activitiesUser->value3) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($activitiesUser->id) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Com') ?></h4>
        <?= $this->Text->autoParagraph(h($activitiesUser->com)); ?>
    </div>
</div>
