<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<!-- Optional theme -->

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<div class="container">
    <?php foreach ($items as $item):?>
        <div class="panel panel-primary">
            <div class="panel-heading"><h3><strong><?= $item->activity["title"] ?></strong></h3>  <?= $item->user["username"] ?></div>
            <div class="panel-body">
                <?php if ($item->file1):?>
                    <img src="<?=DS."files".DS."ActivitiesUsers".DS."file1".DS.$item->file1?>">
            <?php endif;?>
                <h4><?= $item->com ?></h4>
            </div>
        </div>

    <?php endforeach;?>
</div>

<script>
    setTimeout(function(){
        window.location.reload(1);
    }, 5000);
</script>