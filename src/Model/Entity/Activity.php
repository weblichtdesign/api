<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Activity Entity
 *
 * @property int $id
 * @property string $title
 * @property string $description
 * @property int $activity_category_id
 * @property int $activity_type_id
 * @property string $value1
 * @property string $value3
 * @property string $value4
 * @property string $file1
 * @property string $file2
 * @property string $value2
 * @property int $points
 * @property string $long
 * @property string $lat
 *
 * @property \App\Model\Entity\ActivityCategory $activity_category
 * @property \App\Model\Entity\ActivityType $activity_type
 * @property \App\Model\Entity\User[] $users
 */
class Activity extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
