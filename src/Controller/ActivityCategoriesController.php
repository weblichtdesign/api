<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * ActivityCategories Controller
 *
 * @property \App\Model\Table\ActivityCategoriesTable $ActivityCategories
 *
 * @method \App\Model\Entity\ActivityCategory[] paginate($object = null, array $settings = [])
 */
class ActivityCategoriesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $activityCategories = $this->paginate($this->ActivityCategories);

        $this->set(compact('activityCategories'));
        $this->set('_serialize', ['activityCategories']);
    }

    /**
     * View method
     *
     * @param string|null $id Activity Category id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $activityCategory = $this->ActivityCategories->get($id, [
            'contain' => ['Activities']
        ]);

        $this->set('activityCategory', $activityCategory);
        $this->set('_serialize', ['activityCategory']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $activityCategory = $this->ActivityCategories->newEntity();
        if ($this->request->is('post')) {
            $activityCategory = $this->ActivityCategories->patchEntity($activityCategory, $this->request->getData());
            if ($this->ActivityCategories->save($activityCategory)) {
                $this->Flash->success(__('The activity category has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The activity category could not be saved. Please, try again.'));
        }
        $this->set(compact('activityCategory'));
        $this->set('_serialize', ['activityCategory']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Activity Category id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $activityCategory = $this->ActivityCategories->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $activityCategory = $this->ActivityCategories->patchEntity($activityCategory, $this->request->getData());
            if ($this->ActivityCategories->save($activityCategory)) {
                $this->Flash->success(__('The activity category has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The activity category could not be saved. Please, try again.'));
        }
        $this->set(compact('activityCategory'));
        $this->set('_serialize', ['activityCategory']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Activity Category id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $activityCategory = $this->ActivityCategories->get($id);
        if ($this->ActivityCategories->delete($activityCategory)) {
            $this->Flash->success(__('The activity category has been deleted.'));
        } else {
            $this->Flash->error(__('The activity category could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
