<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 *
 * @method \App\Model\Entity\User[] paginate($object = null, array $settings = [])
 */
class UsersController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $users = $this->paginate($this->Users);

        $this->set(compact('users'));
        $this->set('_serialize', ['users']);
    }

    /**
     * View method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => ['Activities']
        ]);

        $this->set('user', $user);
        $this->set('_serialize', ['user']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $user = $this->Users->newEntity();
        if ($this->request->is('post')) {
            $data = $this->request->getData();
            $data['unique_id'] = uniqid('', true);
            $user = $this->Users->patchEntity($user, $data);
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
        $activities = $this->Users->Activities->find('list', ['limit' => 200]);
        $this->set(compact('user', 'activities'));
        $this->set('_serialize', ['user']);
    }

    /**
     * Edit method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => ['Activities']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
        $activities = $this->Users->Activities->find('list', ['limit' => 200]);
        $this->set(compact('user', 'activities'));
        $this->set('_serialize', ['user']);
    }

    /**
     * Delete method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $user = $this->Users->get($id);
        if ($this->Users->delete($user)) {
            $this->Flash->success(__('The user has been deleted.'));
        } else {
            $this->Flash->error(__('The user could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
    public function score(){
        $data = $this->request->data;
        $user= $this->Users->get($data['id'],[
            'contain' =>['Activities']]);
        $score = 0;
        foreach ($user['activities'] as $act){
            $score += $act['points'];
        }
        $jResult = json_encode($score);


        $this->response->type('json');
        $this->response->body($jResult);

        //
        return $this->response;
    }

    public function register(){
        if ($this->request->is('post')) {
            $user = $this->Users->newEntity();
            $data = $this->request->data;
            if($this->Users->find()
            ->where(['username like' => $data['username']])
            ->first()){
                $jResult = json_encode(["success" => 0, "error" => 2, "error_msg" => "The username "
                    .$data['username']." already exist."]);


                $this->response->type('json');
                $this->response->body($jResult);

                //
                return $this->response;

            }
            $data['unique_id'] = uniqid('', true);
            $user = $this->Users->patchEntity($user, $data);
            if ($this->Users->save($user)) {
                $response['error'] = FALSE;
                $response['uid'] = $user['unique_id'];
                unset($user['unique_id']);
                unset($user['password']);
                $response['user'] = $user;


                $jResult = json_encode($response);
                $this->response->type('json');
                $this->response->body($jResult);

                //
                return $this->response;

            }
            $jResult = json_encode(["success" => 0, "error" => 1, "error_msg" => "Unknown error"]);


            $this->response->type('json');
            $this->response->body($jResult);

            //
            return $this->response;


        }

    }

    public function login(){


            $data = $this->request->data;

            $user = $this->Users->find()
                ->where(['username like' => $data['username']])
                ->where(['password like' => $data['password']])
                ->first();

            if($user){
                $response['error'] = FALSE;
                $response['uid'] = $user['unique_id'];
                unset($user['unique_id']);
                unset($user['password']);
                $response['user'] = $user;



            }else{
                $response['error'] = TRUE;
                $response["error_msg"] = "Login credentials are wrong. Please try again!";

            }
            $jResult = json_encode($response);


            $this->response->type('json');
            $this->response->body($jResult);

            //
            return $this->response;


        }



}
